import os
from dotenv import load_dotenv

import cx_Oracle

from oracle_db_manager.db_manager import DatabaseManager


# Загрузка переменных окружения из файла .env
load_dotenv()

# Путь
path = os.getenv('ORC_CLI_PATH')
cx_Oracle.init_oracle_client(lib_dir=fr"{path}")

# Установка параметров подключения
host = os.getenv('HOST')
port = int(os.getenv('PORT'))
sid = os.getenv('SID')
username = os.getenv('ORC_USERNAME')
password = os.getenv('PASSWORD')

# Пример использования класса DatabaseManager с использованием выражения with
with DatabaseManager(host=host, port=port, sid=sid, username=username,
                     password=password) as db_manager:
    # Выполнение SELECT-запроса
    select_query = """
        SELECT * FROM PROC.DH_UCH
        WHERE ROWNUM <= 100
    """
    result = db_manager.execute_query(select_query)
    # Вывод результатов
    for row in result[:10]:
        print(row)

    # # Выполнение INSERT-запроса
    # insert_query = "INSERT INTO your_table (column1, column2) VALUES (:1, :2)"
    # data = ('value1', 'value2')
    # db_manager.execute_update(insert_query, data)
import cx_Oracle


class DatabaseManager:
    def __init__(self, host, port, sid, username, password):
        self.host = host
        self.port = port
        self.sid = sid
        self.username = username
        self.password = password
        self.connection = None
        self.cursor = None

    def __enter__(self):
        dsn = cx_Oracle.makedsn(self.host, self.port, self.sid)
        self.connection = cx_Oracle.connect(self.username, self.password, dsn)
        self.cursor = self.connection.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.cursor is not None:
            self.cursor.close()
        if self.connection is not None:
            self.connection.commit()
            self.connection.close()

    def execute_query(self, query, params=None):
        try:
            if params is not None:
                self.cursor.execute(query, params)
            else:
                self.cursor.execute(query)
            result = self.cursor.fetchall()
            return result
        except cx_Oracle.Error as error:
            print("Error executing query:", error)

    def execute_update(self, query, params=None):
        try:
            if params is not None:
                self.cursor.execute(query, params)
            else:
                self.cursor.execute(query)
            self.connection.commit()
        except cx_Oracle.Error as error:
            print("Error executing update:", error)

    def executemany(self, query, values):
        try:
            self.cursor.executemany(query, values)
            self.connection.commit()
        except cx_Oracle.Error as error:
            print("Error executing many:", error)

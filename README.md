# oracle_db_manager

Использование менеджера подключения к бд Oracle через python 

## Руководство по использованию

Чтобы использовать данный менеджер нужно проделать следующие шаги:

- установить библиотеку в requirements.txt

```
pip install -r requirements.txt
```

- для работы модуля cx_Oracle необходима установка Oracle Instant Client (описано далее)

- создать файл в корне директории _.env_ и добавить туда следующие параметры:

```
HOST=localhost
PORT=1521
SID=your_sid
USERNAME=your_username
PASSWORD=your_password
ORC_CLI_PATH=C:\oracle\instantclient_21_10
```

- Изменить значения параметров на нужные.

## Установка Oracle Instant Client

- скачать последную версию клиента Basic или Basic Light. На момент написания это 21.

- распаковать папку и поместить его например по пути `C:\oracle\instantclient_21_10`.
Сохранить данные путь в переменную окружения _ORC_CLI_PATH_

- надо установить Visual Studio в зависимости от версии Oracle Instant Client. 
Список следующий: 

    - For Instant Client 21 install VS 2019 or later.

    - For Instant Client 19 install VS 2017.

    - For Instant Client 18 or 12.2 install VS 2013

    - For Instant Client 12.1 install VS 2010

    - For Instant Client 11.2 install VS 2005 64-bit

- [ ] [Справочник по установке cx_Oracle](https://cx-oracle.readthedocs.io/en/latest/user_guide/installation.html)
